package co.simplon.chefdoeuvre.gonnet.gonnetf.controller;

import java.security.Principal;

import javax.print.DocFlavor.STRING;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Competences;
import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Player;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.CompRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.ICharaRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.ICompRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.IPlayerRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.PlayerRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.UserPlayerRepo;

@Controller
public class HomeController {
    @Autowired
    ICharaRepo chararepo;

    @Autowired
    private PasswordEncoder encoder;
    
    @Autowired
    ICompRepo compRepo;

    // @Autowired
    // IPlayerRepo playerRepo;

    @Autowired
    UserPlayerRepo playerRepo;

    @Autowired
    private CompRepo repo;

    @GetMapping("/")
    public String homepage(Model model){
        model.addAttribute("user", new Player());

        
        return "authentification";
    }

    // @GetMapping("/authentification")
    // public String AuthPage(Model model){
    //     model.addAttribute("user", new Player());

    //     return "authentification";
    // }

    @PostMapping("/authentification")
    public String registerUser(@Valid Player user, BindingResult result, Model model) {
        
        if(result.hasErrors()) {
            return "authentification";
        }
        if(playerRepo.findByEmail(user.getEmail()) != null) {
            model.addAttribute("feedback", "User already exists");
            
            return "authentification";
        }
        //user.setPseudo("test");
        String hashedPassword = encoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setRole("ROLE_USER");
        playerRepo.save(user);
        return "redirect:index";
    }

    @GetMapping("/index")
    public String logHomepage(){

        
        return "index";
    }

    @GetMapping("/player")
    public String playerPage(Model model){

        model.addAttribute("players", playerRepo.findAll());
        return "players";

    }

    @GetMapping("/login")
    public String loginPage(Model model){

        model.addAttribute("user", new Player());

        return "login";
    }

    // @PostMapping("login")
    // public String loginPageConfirm(@Valid Player user, Model model, BindingResult result, @AuthenticationPrincipal Player player){
    //     String hashedPassword = encoder.encode(user.getPassword());
        
    //     if(result.hasErrors()) {
    //         return "login";
    //     }
    //     if((playerRepo.PlayerAutenthification(user.getPseudo(), hashedPassword))==null) {
    //         model.addAttribute("feedback", "User does not exists");
            
    //         return "login";
    //     }
    

    //     return "redirect:index";

    // }
    

}

package co.simplon.chefdoeuvre.gonnet.gonnetf.repository;

import java.util.List;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Player;

public interface IPlayerRepo {

    List<Player> findAll();
    Player findById(Integer id);
    void save(Player chara);
    void update(Player chara);
    void deleteById(Integer id);

}
    


package co.simplon.chefdoeuvre.gonnet.gonnetf.entities;

public class charaToPlayer {
    private int idplayer;
    private int idChar;
   
   
    public charaToPlayer(int idplayer, int idChar) {
        this.idplayer = idplayer;
        this.idChar = idChar;
    }
   
    public int getIdplayer() {
        return idplayer;
    }
    public void setIdplayer(int idplayer) {
        this.idplayer = idplayer;
    }
    public int getIdChar() {
        return idChar;
    }
    public void setIdChar(int idChar) {
        this.idChar = idChar;
    }
    
}

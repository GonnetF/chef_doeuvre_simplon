package co.simplon.chefdoeuvre.gonnet.gonnetf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Player;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.UserPlayerRepo;

@Controller
public class PlayerControler {
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UserPlayerRepo repo;

    @GetMapping("/register")
    public String showRegister(Model model) {
        model.addAttribute("user", new Player());

        
        return "register";
    }
}

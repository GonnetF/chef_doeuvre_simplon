package co.simplon.chefdoeuvre.gonnet.gonnetf.repository;

import java.util.List;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Characters;
import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Competences;

public interface ICharaRepo {
    
    List<Characters> findAll();
    Characters findById(Integer id);
    void save(Characters chara);
    void update(Characters chara);
    void deleteById(Integer id);

    void addComp(Characters charac, Competences comp);
    List<Competences> CharIDToComp(int idchara);

}

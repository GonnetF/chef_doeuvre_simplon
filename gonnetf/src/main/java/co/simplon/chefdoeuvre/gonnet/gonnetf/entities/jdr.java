package co.simplon.chefdoeuvre.gonnet.gonnetf.entities;

public class jdr {
    private int id_jdr;
    private String jdr_title;
    private String jdr_synopsys;
    
    public jdr(int id_jdr, String jdr_title) {
        this.id_jdr = id_jdr;
        this.jdr_title = jdr_title;
    }

    public int getId_jdr() {
        return id_jdr;
    }

    public void setId_jdr(int id_jdr) {
        this.id_jdr = id_jdr;
    }

    public String getJdr_title() {
        return jdr_title;
    }

    public void setJdr_title(String jdr_title) {
        this.jdr_title = jdr_title;
    }

    public String getJdr_synopsys() {
        return jdr_synopsys;
    }

    public void setJdr_synopsys(String jdr_synopsys) {
        this.jdr_synopsys = jdr_synopsys;
    }
    
}

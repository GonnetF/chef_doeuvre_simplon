package co.simplon.chefdoeuvre.gonnet.gonnetf.entities;

public class charToJdr {
    private int id_jdr;
    private int id_char;
    public charToJdr(int id_jdr, int id_char) {
        this.id_jdr = id_jdr;
        this.id_char = id_char;
    }
    public int getId_jdr() {
        return id_jdr;
    }
    public void setId_jdr(int id_jdr) {
        this.id_jdr = id_jdr;
    }
    public int getId_char() {
        return id_char;
    }
    public void setId_char(int id_char) {
        this.id_char = id_char;
    }

    
    
    
}

package co.simplon.chefdoeuvre.gonnet.gonnetf.repository;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Competences;

@Repository
public class CompRepo implements ICompRepo{

    @Autowired
    DataSource datasourcue;
    Connection connection;


    @Override
    public List<Competences> findAll(){
        List<Competences> CompetencesList = new ArrayList<>();
        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM Competences");
            ResultSet result =stm.executeQuery();

            while (result.next()){
                Competences competences = new Competences(
                result.getInt("id"),
                result.getString("title"),
                result.getString("effect")
                );

                
                CompetencesList.add(competences);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  CompetencesList;
    }

    @Override
    public Competences findById(Integer id) {
        PreparedStatement stm;
        try {
            stm = connection.prepareStatement("SELECT * FROM Competencess WHERE comp_id = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            result.next();

            Competences competences = new Competences(
                result.getInt("comp_id"),
                result.getString("comp_title"),
                result.getString("comp_effect")
                );

            return competences;


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

    @Override
    public void save(Competences Competences) {
       
            PreparedStatement stm;
            try {
                stm = connection.prepareStatement("INSERT INTO Competencess VALUES(null, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
            
             //on utilise les set de preparedstatement pour eviter les injection sql
             stm.setString(1, Competences.getTitle());
             stm.setString(2, Competences.getEffect());

             stm.executeUpdate();


            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        
    }


    @Override
    public void update(Competences Competences) {
        try {
            PreparedStatement stm =  connection.prepareStatement("UPDATE Competencess SET comp_title=?, comp_effect=?", PreparedStatement.RETURN_GENERATED_KEYS);
            stm.setString(1, Competences.getTitle());
            stm.setString(2, Competences.getEffect());            
       
            stm.executeUpdate();

       
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

   
        
    }


    @Override
    public void deleteById(Integer id) {

        try {
            PreparedStatement stm = connection.prepareStatement("DELETE * FROM Competencess WHERE id=?");
            stm.setInt(1, id);

            stm.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        
    }

   

}



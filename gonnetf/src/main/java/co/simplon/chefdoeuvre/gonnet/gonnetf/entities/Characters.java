package co.simplon.chefdoeuvre.gonnet.gonnetf.entities;

public class Characters {
    private int char_idChar;
    private String char_Fname;
    private String char_Lname;
    private boolean char_status;
    private int char_FOR;
    private int char_MEN;
    private int char_SOC;
    private String char_bg;
    
    public Characters(){

    }
    public Characters(String charFname, String charLname, boolean status, int fOR, int mEN, int sOC) {
        char_Fname = charFname;
        char_Lname = charLname;
        char_status = status;
        char_FOR = fOR;
        char_MEN = mEN;
        char_SOC = sOC;
    }
    public Characters(int CHarid, String charFname, String charLname, boolean status, int fOR, int mEN, int sOC) {
        char_idChar = CHarid;
        char_Fname = charFname;
        char_Lname = charLname;
        char_status = status;
        char_FOR = fOR;
        char_MEN = mEN;
        char_SOC = sOC;
    }
  
    public int getIdChar() {
        return char_idChar;
    }
    public void setIdChar(int idChar) {
        char_idChar = idChar;
    }
    public String getCharFname() {
        return char_Fname;
    }
    public void setCharFname(String charFname) {
        char_Fname = charFname;
    }
    public String getCharLname() {
        return char_Lname;
    }
    public void setCharLname(String charLname) {
        char_Lname = charLname;
    }
    public boolean isStatus() {
        return char_status;
    }
    public void setStatus(boolean status) {
        char_status = status;
    }
    public int getFOR() {
        return char_FOR;
    }
    public void setFOR(int fOR) {
        char_FOR = fOR;
    }
    public int getMEN() {
        return char_MEN;
    }
    public void setMEN(int mEN) {
        char_MEN = mEN;
    }
    public int getSOC() {
        return char_SOC;
    }
    public void setSOC(int sOC) {
        char_SOC = sOC;
    }
    public String getBg() {
        return char_bg;
    }
    public void setBg(String bg) {
        char_bg = bg;
    }


}

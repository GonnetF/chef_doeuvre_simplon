package co.simplon.chefdoeuvre.gonnet.gonnetf.entities;

import org.springframework.beans.factory.config.PlaceholderConfigurerSupport;

public class Competences {
    private int comp_id;
    private String comp_title;
    private String comp_effect; 
    
    public Competences(int id, String title, String effect) {
        comp_id = id;
        comp_title = title;
        comp_effect = effect;
    }

    public Competences(String title, String effect) {
        comp_title = title;
        comp_effect = effect;
    }

    public Competences() {
        comp_title = "placeholder title";
        comp_effect = "placeholder effect";
    }

    public int getId() {
        return comp_id;
    }

    public void setId(int id) {
        comp_id = id;
    }

    public String getTitle() {
        return comp_title;
    }

    public void setTitle(String title) {
        comp_title = title;
    }

    public String getEffect() {
        return comp_effect;
    }

    public void setEffect(String effect) {
        comp_effect = effect;
    }
    
    
}

package co.simplon.chefdoeuvre.gonnet.gonnetf.entities;

import java.util.Collection;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;


public class Player implements UserDetails{
    private int id;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Size(min = 4, max = 45)
    private String password;
    private String pseudo;
    private String role;
    
    public Player() {
    }
    public Player(int id, String pseudo) {
        this.id = id;
        this.pseudo = pseudo;
    }
    public Player(int id, String pseudo, String password, String Email, String role) {
        this.password = password;
        this.email = Email;
        this.id = id;
        this.pseudo = pseudo;
        this.role = "ROLE_USER";
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getPseudo() {
        return pseudo;
    }
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(
            new SimpleGrantedAuthority("ROLE_USER")
        );
    }
    
    public String getPassword() {
        return password;
    }
    public String getUsername() {
        return pseudo;
    }
    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }
    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    

    

    
}

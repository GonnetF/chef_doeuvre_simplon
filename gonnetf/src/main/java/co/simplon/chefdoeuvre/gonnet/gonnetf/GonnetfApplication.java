package co.simplon.chefdoeuvre.gonnet.gonnetf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Characters;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.CharaRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.CompRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.ICharaRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.ICompRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.IPlayerRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.PlayerRepo;

@SpringBootApplication
public class GonnetfApplication {

	public static void main(String[] args) {
		SpringApplication.run(GonnetfApplication.class, args);		
		
	}

}

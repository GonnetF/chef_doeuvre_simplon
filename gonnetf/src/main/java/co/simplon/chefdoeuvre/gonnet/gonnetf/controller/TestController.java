package co.simplon.chefdoeuvre.gonnet.gonnetf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Characters;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.CharaRepo;
import co.simplon.chefdoeuvre.gonnet.gonnetf.repository.ICharaRepo;

@Controller
public class TestController {
    @Autowired
    ICharaRepo chararepo;


    @GetMapping("/test")
    public String SaveTest(){
        

        Characters character = new Characters("test", "supertestquitue", true, 75, 55, 45);
		
		//ICompRepo comprepo = new CompRepo();
		//IPlayerRepo playerepo = new PlayerRepo();

		chararepo.save(character);

        return "index";
    }
}

package co.simplon.chefdoeuvre.gonnet.gonnetf.repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Player;

/**
 * Ce repository nous permettra de rajouter un user en base de données, mais
 * aussi de récupérer un User par son email, afin de vérifier si un email est déjà
 * pris à l'inscription ou de récupérer un User dans le cas d'un login
 * On lui fait également implémenter l'interface UserDetailsService qui rajoute
 * une méthode loadUserByUsername dont Spring a besoin pour récupérer un User
 */
@Repository
public class UserPlayerRepo implements UserDetailsService{
        @Autowired
    private DataSource dataSource;
    private Connection connection;

    public List<Player> findAll() {
        List<Player> list = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM players");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Player(
                                result.getInt("id_player"),
                                result.getString("pseudo"),
                                result.getString("password"),
                                result.getString("email"),
                                result.getString("role")
                            )
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    public Player PlayerAutenthification(String pseudo, String Password){

        try{
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
            .prepareStatement("SELECT * FROM players WHERE pseudo = ? AND passworld = ?");

            stmt.setString(1, pseudo);
            stmt.setString(2, Password);

            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Player(
                    result.getInt("id_player"),
                    result.getString("pseudo"),
                    result.getString("password"),
                    result.getString("email"),
                    result.getString("role")
                );
            }
            
        }catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return null;

    }
    public boolean update(Player user) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE players SET email=?,password=?,pseudo=?, role=? WHERE id=?");

            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getPseudo());
            stmt.setString(4, user.getRole());
            stmt.setInt(5, user.getId());
            
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }

    /**
     * Méthode qui ajoute un user en base de données
     * 
     * @param user Le user à ajouter en base de données
     * @return true si ça à marcher, false sinon
     */
    public boolean save(Player user) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO players (pseudo, password, email, role) VALUES (?,?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(3, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(1, user.getPseudo());
            stmt.setString(4, user.getRole());
            
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                user.setId(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }

    /**
     * Méthode permettant de récupérer un user en se basant sur l'email
     * 
     * @param email l'email de l'user recherché
     * @return Le user correspondant à l'email ou null si pas de user
     */
    public Player findByEmail(String email) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM players WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Player(
                    result.getInt("id_player"),
                    result.getString("pseudo"),
                    result.getString("password"),
                    result.getString("email"),
                    result.getString("role")
                );

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    public Player findByPseudo(String pseudo) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM players WHERE pseudo=?");
            stmt.setString(1, pseudo);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Player(
                    result.getInt("id_player"),
                    result.getString("pseudo"),
                    result.getString("password"),
                    result.getString("email"),
                    result.getString("role")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }


    public Player findByid(int id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM players WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Player(
                        result.getInt("id_player"),
                        result.getString("pseudo"),
                        result.getString("password"),
                        result.getString("email"),
                        result.getString("role"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    /**
     * La méthode de l'interface pour Spring, au final dedans on ne fait qu'appeler notre
     * méthode findBypseudo (vu que dans notre cas notre pseudo est notre username)
     * 
     * @param username Le username (ici le pseudo) du user recherché
     * @return Le User correspondant au pseudo
     * @throws UsernameNotFoundException On fait en sorte de déclencher une
     *                                   exception si aucun User ne correspond à
     *                                   l'email donné (c'est spring qui veut ça)
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Player user = findByPseudo(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
    

}

package co.simplon.chefdoeuvre.gonnet.gonnetf.entities;

public class compToChar {
    private int ctc_idComp;
    private int ctc_idChar;

    
    public compToChar(int idComp, int idChar) {
        ctc_idComp = idComp;
        ctc_idChar = idChar;
    }
    public int getIdComp() {
        return ctc_idComp;
    }
    public void setIdComp(int idComp) {
        ctc_idComp = idComp;
    }
    public int getIdChar() {
        return ctc_idChar;
    }
    public void setIdChar(int idChar) {
        ctc_idChar = idChar;
    }
    
    
}

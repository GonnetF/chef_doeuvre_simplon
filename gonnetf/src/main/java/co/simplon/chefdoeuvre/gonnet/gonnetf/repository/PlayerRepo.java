package co.simplon.chefdoeuvre.gonnet.gonnetf.repository;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Player;

@Repository
public class PlayerRepo implements IPlayerRepo{

    @Autowired
    DataSource datasourcue;
    Connection connection;


    @Override
    public List<Player> findAll(){
        List<Player> playercterList = new ArrayList<>();
        try {
            PreparedStatement stm = datasourcue.getConnection()
            .prepareStatement("SELECT * FROM players");
            ResultSet result =stm.executeQuery();
            while (result.next()){
                Player player = new Player(
                result.getInt("id_player"),
                result.getString("Pseudo")
                
                );

                playercterList.add(player);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  playercterList;
    }

    @Override
    public Player findById(Integer id) {
        PreparedStatement stm;
        try {
            stm = connection.prepareStatement("SELECT * FROM players WHERE id_player = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            result.next();

            Player player = new Player(
                result.getInt("id_player"),
                result.getString("Pseudo")
                );

            return player;


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

    @Override
    public void save(Player player) {
       
            PreparedStatement stm;
            try {
                stm = connection.prepareStatement("INSERT INTO players VALUES(null, ?)", PreparedStatement.RETURN_GENERATED_KEYS);

                stm.executeUpdate();

            
             //on utilise les set de preparedstatement pour eviter les injection sql
             stm.setString(1, player.getPseudo());
            

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        
    }


    @Override
    public void update(Player player) {
        try {
            PreparedStatement stm =  connection.prepareStatement("UPDATE players SET pseudo=?", PreparedStatement.RETURN_GENERATED_KEYS);
            stm.setString(1, player.getPseudo());
            
            stm.executeUpdate();

       
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

   
        
    }


    @Override
    public void deleteById(Integer id) {

        try {
            PreparedStatement stm = connection.prepareStatement("DELETE * FROM players WHERE id=?");
            stm.setInt(1, id);

            stm.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        
    }

   

}

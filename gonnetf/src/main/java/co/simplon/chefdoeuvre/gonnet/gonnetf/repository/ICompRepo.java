package co.simplon.chefdoeuvre.gonnet.gonnetf.repository;

import java.util.List;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Competences;

public interface ICompRepo {
    
    List<Competences> findAll();
    Competences findById(Integer id);
    void save(Competences chara);
    void update(Competences chara);
    void deleteById(Integer id);
    
    
}

package co.simplon.chefdoeuvre.gonnet.gonnetf.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Characters;
import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Competences;
import co.simplon.chefdoeuvre.gonnet.gonnetf.entities.Player;

@Repository
public class CharaRepo implements ICharaRepo {
    
    @Autowired
    private DataSource dataSource;



    @Override
    public List<Characters> findAll(){
        List<Characters> characterList = new ArrayList<>();
        try {
            PreparedStatement stm = dataSource.getConnection()
            .prepareStatement("SELECT * FROM characters");


            ResultSet result =stm.executeQuery();

            while (result.next()){
                Characters chara = new Characters(
                result.getInt("id_chara"),
                result.getString("char_Fname"),
                result.getString("char_Lname"),
                result.getBoolean("char_Status"),
                result.getInt("cFOR"),
                result.getInt("cMEN"),
                result.getInt("cSOC")
                );
                
                characterList.add(chara);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  characterList;
    }

    @Override
    public Characters findById(Integer id) {
        PreparedStatement stm;
        try {
            stm = dataSource.getConnection()
            .prepareStatement("SELECT * FROM characters WHERE id_chara = ?");
            stm.setInt(1, id);
            ResultSet result = stm.executeQuery();
            result.next();

            Characters chara = new Characters(
                result.getInt("id"),
                result.getString("char_Fname"),
                result.getString("char_Lname"),
                result.getBoolean("char_Status"),
                result.getInt("cFOR"),
                result.getInt("cMEN"),
                result.getInt("cSOC")
                );
                stm.executeUpdate();


            return chara;


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

    @Override
    public void save(Characters chara) {
       
            PreparedStatement stm;
            try {
                stm = dataSource.getConnection()
                .prepareStatement("INSERT INTO characters VALUES(null, ?,?,?,?,?,?,\"\")", PreparedStatement.RETURN_GENERATED_KEYS);
            
            
             //on utilise les set de preparedstatement pour eviter les injection sql
             stm.setString(1, chara.getCharFname());
             stm.setString(2, chara.getCharLname());
             stm.setBoolean(3, chara.isStatus());
             stm.setInt(4, chara.getFOR());
             stm.setInt(5, chara.getMEN());
             stm.setInt(6, chara.getSOC());

             stm.executeUpdate();
            
             System.out.println("test crea");

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        
    }


    @Override
    public void update(Characters chara) {
        try {
            PreparedStatement stm =  dataSource.getConnection()
            .prepareStatement("UPDATE characters SET char_Fname=?, char_Lname=?, char_Status=?, cFOR=?, cMEN=?,cSOC=?, char_bg=?", PreparedStatement.RETURN_GENERATED_KEYS);
            stm.setString(1, chara.getCharFname());
            stm.setString(2, chara.getCharLname());
            stm.setBoolean(3, chara.isStatus());
            stm.setInt(4, chara.getFOR());
            stm.setInt(5, chara.getMEN());
            stm.setInt(6, chara.getSOC());
       
            stm.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
      
    }

    @Override
    public void deleteById(Integer id) {

        try {
            PreparedStatement stm = dataSource.getConnection()
            .prepareStatement("DELETE * FROM characters WHERE id=?");
            stm.setInt(1, id);

            stm.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        
    }

    @Override
    public void addComp(Characters charac, Competences comp) {
        try {
            PreparedStatement stm = dataSource.getConnection().prepareStatement("INSERT INTO charToComp VALUES (?,?)") ;
            stm.setInt(1, comp.getId());
            stm.setInt(2, charac.getIdChar());

            stm.executeUpdate();


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
   


    @Override
    public List<Competences> CharIDToComp(int idchara) {

        try {
            PreparedStatement stm = dataSource.getConnection()
            .prepareStatement(
                "SELECT * FROM competences c INNER JOIN charToComp ch ON c.comp_id = ch.comp_id WHERE ch.chara_id = ?");
                stm.setInt(1, idchara);
                ResultSet result =stm.executeQuery();


                while(result.next()){
                    Competences comp = new Competences(
                        result.getInt("comp_id"),
                        result.getString("comp_title"),
                        result.getString("comp_effect"));
                }

                stm.executeUpdate();


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("çamarchpa CHarIDToComp");
        return null;
    }

    public Player GetCharPlayer(Characters chara){
        try {
            PreparedStatement stm = dataSource.getConnection()
            .prepareStatement("SELECT * FROM players p INNER JOIN charToPlayer cp ON cp.player_id = p.id_player WHERE cp.chara_id = ?");
            
            stm.setInt(1, chara.getIdChar());

            ResultSet result = stm.executeQuery();
            Player player = new Player(
                result.getInt("id_player"),
                result.getString("pseudo"));


                stm.executeUpdate();

                return player;

                

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("çamarchpa getcharpLAYER");
        return null;

    }
}

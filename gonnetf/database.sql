DROP DATABASE IF EXISTS rp_database;

CREATE DATABASE IF NOT EXISTS rp_database
    DEFAULT CHARACTER SET = 'utf8mb4';



DROP TABLE IF EXISTS charToComp;
DROP TABLE IF EXISTS charToPlayer;
DROP TABLE IF EXISTS players;
DROP TABLE IF EXISTS characters;
DROP TABLE IF EXISTS competences;
DROP TABLE IF EXISTS users;

DROP TABLE IF EXISTS authorities;



CREATE TABLE users{
    iduser INTEGER AUTO_INCREMENT PRIMARY KEY,

};

CREATE TABLE players(
    id_player INTEGER AUTO_INCREMENT PRIMARY KEY ,
    pseudo VARCHAR(50),
    password VARCHAR(255) NOT NULL,
    email VARCHAR(100) NOT NULL,
    role VARCHAR(64)

);

CREATE TABLE authorities (
  `id` INT NOt NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `authority` VARCHAR(45) NOT NULL,
  PRIMARY KEY(`id`)
);

CREATE TABLE characters(
    id_chara INTEGER AUTO_INCREMENT PRIMARY KEY,
    char_Fname VARCHAR(30) NOT NULL,
    char_Lname VARCHAR(30),
    char_Status BOOLEAN NOT NULL DEFAULT 1,
    cFOR INTEGER NOT NULL DEFAULT 0,
    cMEN INTEGER NOT NULL DEFAULT 0,
    cSOC INTEGER NOT NULL DEFAULT 0,
    char_bg VARCHAR(5000)
);

CREATE TABLE competences(
    comp_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    comp_title VARCHAR(30),
    comp_effect VARCHAR(255)
);

CREATE TABLE charToPlayer(
    player_id INTEGER NOT NULL,
    chara_id INTEGER NOT NULL,

    CONSTRAINT fk_char_player FOREIGN KEY (player_id)
    REFERENCES players(id_player),
    CONSTRAINT fk_player_char FOREIGN KEY (chara_id)
    REFERENCES characters(id_chara)
    
);

CREATE TABLE charToComp(
    comp_id INTEGER NOT NULL,
    chara_id INTEGER NOT NULL PRIMARY KEY,
    CONSTRAINT fk_char_comp FOREIGN KEY (comp_id)
    REFERENCES competences(comp_id),
    CONSTRAINT fk_comp_chara FOREIGN KEY (chara_id)
    REFERENCES characters(id_chara)
);


 



